//
//  WeatherModel.swift
//  Clima
//
//  Created by Leah Joy Ylaya on 12/14/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

struct WeatherModel {
    let conditionId: Int
    let cityName: String
    let temperature: Double
    
    var temperatureString: String {
        return String(format: "%.1f", temperature)
    }
    
    var conditionName: String {
        switch conditionId {
        case 200...232:
            return "cloud.bolt"
        case 300...321:
            return "cloud.drizzle"
        case 500...504:
            return "cloud.sun.rain"
        case 511...531:
            return "cloud.rain"
        case 600...622:
            return "cloud.snow"
        case 701...721:
            return "cloud.haze"
        case 731:
            return "cloud.dust"
        case 741:
            return "cloud.fog"
        case 751...771:
            return "cloud.dust"
        case 781:
            return "cloud.tornado"
        case 800:
            return "sun.max"
        case 801:
            return "cloud.sun"
        case 802:
            return "cloud"
        case 803...804:
            return "smoke"
        default:
            return "cloud"
        }
    }
    
}

struct WeatherData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]
}

struct Main: Codable {
    let temp: Double
}

struct Weather: Codable {
    let description: String
    let id: Int
}
